<?php
	session_start();
	include 'serv.php';
	if(isset($_SESSION['usuario'])){
		echo "<div class='navbar-fixed'>
        <nav role='navigation'>
            <div style='background-color: #26a69a;'>
                <div class='nav-wrapper' style='background-color: #26a69a;'>
                <a href='#' id='logo-container' class='brand-logo center flow-text'>Administración</a>
                    <ul id='slide-out' class='side-nav'>
                        <li><a href='logout.php' style='color: #26a69a;'>Salir</a></li>
                    </ul>
                <a href='#' data-activates='slide-out' class='button-collapse show-on-large' id='dale'><i class='mdi-navigation-menu'></i></a>
                </div>
            </div>
        </nav>
    </div>";
   $id_usuario = $_POST['id_usuario'];
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="css/materialize.css" media="screen,projection">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/zoom.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>
<div class="row">
	<?php
		$hoy = date('Y/m/d');
		//echo $hoy;
	?>
	<form method="post" action="vencidosf.php">
		<button class="waves-effect waves-light btn">Comprobar vencidos</button>
	</form>
</div>
<?php 
	$consultnombre = "SELECT nombre_usuario FROM usuarios WHERE id_usuario=$id_usuario";
	$resultado = mysqli_query($connect,$consultnombre);
				while($rowe = mysqli_fetch_array($resultado)){
?>
<div><h2 class="flow-text">Empleados de <?echo $rowe['nombre_usuario'];?></h2></div>
<?}?>
	<table class="responsive-table bordered striped" id="tablaemp2">
			<tr>
				<th>Trabajador</th>
				<th>Ocupación</th>
				<th>Residencia</th>
				<th>IFE Anverso</th>
				<th>IFE Reverso</th>
				<th>Fotografía</th>
				<th>Fecha de vencimiento</th>
				<th>Numero de impresiones</th>
				<th>Dar de baja</th>
				<th>Imprimir</th>
			</tr>
<?php
	
	$sql = "SELECT id_empleado,empleado,ocupacion,residencia,ife_anverso,ife_reverso,fotografia,fecha_final,clicks FROM empleados WHERE id_usuario=$id_usuario";
	$result = mysqli_query($connect,$sql);
				while($row = mysqli_fetch_array($result)){
?>
	<tr>
		<td><? echo $row['empleado']; ?></td>
		<td><? echo $row['ocupacion']; ?></td>
				<td><? echo $row['residencia'];?></td>
				<td><img class="responsive-img fotitos" width="150" height="150" data-action="zoom" src="<?php echo $row['ife_anverso'];?>"></td>
				<td><img class="responsive-img fotitos" width="150" height="150" data-action="zoom" src="<?php echo $row['ife_reverso'];?>"></td>
				<td><img class="responsive-img fotitos" width="150" height="150" data-action="zoom" src="<?php echo $row['fotografia'];?>"></td>
				<td><? echo $row['fecha_final'];?></td>
				<td><?echo $row['clicks'];?></td>
				<td>
					<button class="btn red delete_class" id="<? echo $row['id_empleado']; ?>"><i class="material-icons">delete</i></button>
				</td>
				<td>
					<form method="post" action="imprimiremp.php" target="_blank" style="margin-top: 13px;">
					<input name="id_empleado" type="text" value="<? echo $row['id_empleado'];?>" hidden>
						<button type="submit" class="btn">imprimir</button>
					</form>
				</td>
	</tr>
		
<? } ?></table>
<a class="waves-effect waves-light btn" href="famemp.php" style="float: right; margin-top: 20px;">Volver al panel<i class="material-icons right">exit_to_app</i></a>
<script src="js/jquery-2.2.2.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/picker.js"></script>
<script src="js/picker.time.js"></script>
<script src="js/picker.date.js"></script>
<script src="js/zoom.js"></script>
<script src="js/main.js"></script>
<script>
	 /*$('.conte').click(function (event) {
            event.preventDefault();
            var cont_id = $(this).attr('value');
            $.ajax({
            	data:'id_empleado='+cont_id,
                url: 'addcont.php',
                type: 'get'

            })
        })*/
</script>
</body>

</html>
<?php
	}else{
		echo '<script> window.location="index.php"; </script>';
	}
	$profile = $_SESSION['usuario'];
?>
