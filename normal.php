<?php
	session_start();
	include 'serv.php';
	if(isset($_SESSION['usuario'])){
		echo "<div class='navbar-fixed'>
        <nav role='navigation'>
            <div style='background-color: #26a69a;'>
                <div class='nav-wrapper' style='background-color: #26a69a;'>
                <a href='#' id='logo-container' class='brand-logo center flow-text'>Administración</a>
                    <ul id='slide-out' class='side-nav'>
	                    <li><a href='ingalba.php' style='color: #26a69a;'>Imprimir Gafetes Ing.</a></li>
	                    <li><a href='famemp.php' style='color: #26a69a;'>Imprimir Gafetes Familia</a></li>
	                    <li><a href='logout.php' style='color: #26a69a;'>Salir</a></li>
                    </ul>
                <a href='#' data-activates='slide-out' class='button-collapse show-on-large' id='dale'><i class='mdi-navigation-menu'></i></a>
                </div>
            </div>
        </nav>
    </div>";
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="css/materialize.css" media="screen,projection">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/zoom.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>

<div class="row" style="margin-top: 18px;">
	<div class="col s12">
		<ul class="tabs">
			<li class="tab col s4"><a href="#faming">Comprobar Vencidos</a></li>
			<li class="tab col s4"><a href="#buscar">Buscar por manzana y lote</a></li>
			<li class="tab col s4"><a href="#daralta">Dar de Alta Usuarios</a></li>
			<li class="tab col s4"><a href="#listafaming">Familias y usuarios registrados</a></li>
		</ul>
	</div>
</div>
<div id="faming">
<div class="container">
	<div class="row">
		<div class="col s12 m6">
			<form method="post" action="vencidosf.php">
				<button class="grey lighten-3 waves-effect waves-light btn" style="float: left; color:#26a69a;">Comprobar Empleados de familias vencidos</button>
			</form>
		</div>
		<div class="col s12 m6">
			<form method="post" action="vencidos.php">
				<button class="grey lighten-3 waves-effect waves-light btn" style="float: right; color:#26a69a;">Comprobar Trabajadores de construcción vencidos</button>
			</form>
		</div>

	</div>
</div>

	<div class="container" style="margin-top: 10px;" id="tablausuarios" hidden>
		<h3 class="flow-text center">Familias y empleados</h3>
		<div class="row">
			<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" class="responsive-table bordered  striped">
				<tr>
					<th>Familia</th>
					<th>Empleados</th>
				</tr>
				<?php
					//$sql = "SELECT id_usuario,usuario FROM usuarios WHERE tipo=1";
					//$sqlu = "SELECT * FROM usuarios U  LEFT JOIN empleados E ON U.id_usuario=E.id_usuario LEFT JOIN albaniles A ON A.id_usuario=U.id_usuario WHERE tipo=3";
				$sql = "SELECT id_usuario,nombre_usuario FROM usuarios WHERE tipo=3";
					$result = mysqli_query($connect,$sql);
					while($row = mysqli_fetch_array($result)){
						?>
						<tr>
							<td><? echo $row['nombre_usuario']; ?></td>
							<td>
								<form method="post" action="consultandof.php">
									<input hidden name="id_usuario" value="<? echo $row['id_usuario'];?>">
									<input class="waves-effect waves-light btn" type="submit" value="Ver">
								</form>
							</td>
							<!--<td class="center"><form method="post" action="gafetes/php/pdf/gafetes.php"><button href="#" style="float: left;"class="waves-effect waves-light btn"><i class="material-icons">print</i></button></form></td>-->
						</tr>
					<?}?>
			</table>
		</div>
		
	</div>
		<div class="container" style="margin-top: 10px;" hidden>
		<h3 class="flow-text center">Ingenieros-Arquitectos y Trabajadores</h3>
			<div class="row">
			<a href=""></a>
				<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" class="responsive-table bordered  striped">
				<tr>
					<th>Ingeniero</th>
					<th>Consultar Trabajadores</th>
				</tr>
				<?php
					//$sql = "SELECT id_usuario,usuario FROM usuarios WHERE tipo=1";
					$sql = "SELECT id_usuario,nombre_usuario FROM usuarios WHERE tipo=1";
					$result = mysqli_query($connect,$sql);
					while($row = mysqli_fetch_array($result)){
						
						?>
						<tr>
							<td><? echo $row['nombre_usuario']; ?></td>
							<td>
								<form method="post" action="consultando.php">
									<input hidden name="id_usuario" value="<? echo $row['id_usuario'];?>">
									<input class="waves-effect waves-light btn" type="submit" value="Ver">
								</form>
							</td>
						</tr>
					<?}?>
			</table>
			</div>
		</div>
		<!--<div class="container" style="margin-top: 10px;">
			<ul class="collapsible" data-collapsible="accordion">
    			<li>
			      <div class="collapsible-header"><i class="material-icons">filter_drama</i>Ingeniero</div>
			      <div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
   	 			</li>
			    <li>
			      <div class="collapsible-header"><i class="material-icons">place</i>Second</div>
			      <div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
			    </li>
			    <li>
			      <div class="collapsible-header"><i class="material-icons">whatshot</i>Third</div>
			      <div class="collapsible-body"><p>Lorem ipsum dolor sit amet.</p></div>
			    </li>
  			</ul>
		</div>-->
			
		
	<div class="container">
		<div class="row">
			<a href="logout.php" style="float: right;"><button class="waves-effect waves-light btn red">Salir<i class="material-icons right">exit_to_app</i></button></a>
		</div>
	</div>
	
</div>	

<div id="daralta">
<div class="flow-text center" style="color: #26a69a">Formulario para dar de Alta Usuario</div>
	<div class="container">
	<div class="row">
	<form class="col s12" method="post" action="addusu.php">
		<div class="row">
			<div class="input-field col s12">
				    <select name="tipo" required>
				      <option value="" disabled selected>Selecciona el tipo de Usuario</option>
				      <option value="1">Ingeniero</option>
				      <option value="3">Familia</option>
				      <option value="2">Administrador</option>
				    </select>
	    		<label></label>
	  		</div>
	  		<div class="row">
	  			<div class="input-field col s12">
		  			<input id="nombre_usu" type="text" class="validate" name="nombre_usu" required>
		  			<label for="nombre_usu">Nombre de usuario</label>
	  			</div>
	  		</div>
	  		<div class="row">
	  			<div class="input-field col s12">
		  			<input id="nombre_propio" type="text" class="validate" name="nombre_propio" required>
		  			<label for="nombre_propio">Nombre y Apellido</label>
	  			</div>
	  		</div>
	  		<div class="row">
	  			<div class="input-field col s12">
		  			<input id="pass_usu" type="text" class="validate" name="pass_usu" required>
		  			<label for="pass_usu">Contraseña para Usuario</label>
	  			</div>
	  		</div>
	  		<div class="row">
	  			<div class="input-field col s12">
		  			<input id="pass_usur" type="text" class="validate" name="pass_usur" required>
		  			<label for="pass_usur">Repita la contraseña</label>
	  			</div>
	  		</div>
	  		<div class="row">
	  			<div class="input-field col s12">
		  			<input id="correo" type="email" class="validate" name="correo" required>
		  			<label for="correo">Correo electrónico</label>
	  			</div>
	  		</div>
		</div>
		<div class="container">
			<div class="row">
				<button type="submit" style="float: right;" class="waves-effect waves-light btn">Registrar<i class="material-icons right">send</i></button>
			</div>
		</div>
			
	</form>	
	</div>
	</div>
</div>
<div id="listafaming">
	
</div>

<!--modal de edición-->
	<div class="modal bottom-sheet" id="modal1">
	    <div class="modal-content">
	      <h4 class="flow-text">Editar Usuario</h4>
	    </div>
	   <form id="frmusuarios" class="col s12" method="post">
	   		<div class="input-field col s12 m6" hidden>
				<input type="text" id="id_usuario" name="id_usuario">
			</div>
			<div class="input-field col s12 m6">
				<input type="text" id="usuario" name="usuario">
			</div>
			<div class="input-field col s12 m6">
				<input type="text" id="name_usuario" name="nombre_usuario">
			</div>
			<div class="input-field col s12 m6">
				<input type="text" id="correo_usuario" name="correo_usuario">
			</div>
		    <div class="modal-footer">
		      <button type="submit" class=" modal-action modal-close waves-effect waves-green btn">Actualizar<i class="material-icons right">thumb_up</i></button>
		    </div>
	    </form>
	</div>

	<div id="buscar"> 
		<form method="POST" action="" onSubmit="return validarForm(this)">
		    <input type="text" placeholder="Buscar por Lote o Manzana" name="lote">
		    <input type="submit" value="Buscar" name="buscar" class="btn">
		</form>

		<script type="text/javascript">
		    function validarForm(formulario) 
		    {
		        if(formulario.lote.value.length==0) 
		        { //¿Tiene 0 caracteres?
		            formulario.lote.focus();  // Damos el foco al control
		            alert('Debes rellenar este campo'); //Mostramos el mensaje
		            return false; 
		         } //devolvemos el foco  
		         return true; //Si ha llegado hasta aquí, es que todo es correcto 
		     }   
		</script>
			<?php  
				//si existe una petición
			 	error_reporting (0);
				$busqueda  = $_POST['buscar'];
				if($busqueda) 
				{   
			?>
		   <!-- el resultado de la búsqueda lo encapsularemos en un tabla -->
		   <table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" class="responsive-table bordered  striped">
		       <tr>
		            <!--creamos los títulos de nuestras dos columnas de nuestra tabla -->
		            <td width="100" align="center"><strong>Nombre</strong></td>
		            <td width="100" align="center"><strong>Ocupacion</strong></td>
		            <td width="100" align="center"><strong>Residencia</strong></td>
		            <td width="100" align="center"><strong>Manzana</strong></td>
		            <td width="100" align="center"><strong>Lote</strong></td>
		            <td width="100" align="center"><strong>Fecha Final</strong></td>
		       </tr> 
		       <?php
			       $buscar = $_POST["lote"];
			     
			       $consulta_mysql= mysqli_query ($connect,"SELECT * FROM albaniles WHERE lote LIKE '%$buscar%' or manzana LIKE '%$buscar%'");
			 
			       while($registro = mysqli_fetch_assoc($consulta_mysql)) 
		       {
		       ?> 
		           <tr>
		               <td class="estilo-tabla" align="center"><?=$registro['albanil']?></td>
		               <td class=”estilo-tabla” align="center"><?=$registro['ocupacion']?></td>
		               <td class="estilo-tabla" align="center"><?=$registro['residencia']?></td>
		               <td class=”estilo-tabla” align="center"><?=$registro['manzana']?></td>
		               <td class="estilo-tabla" align="center"><?=$registro['lote']?></td>
		               <td class=”estilo-tabla” align="center"><?=$registro['fecha_final']?></td>
		           </tr> 
		           <?php 
		       } //fin blucle
		    ?>
		    </table>
		    <?php
		} // fin if  
		?>
	</div>
<!--termina modal de edición-->
<script src="js/jquery-2.2.2.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/picker.js"></script>
<script src="js/picker.time.js"></script>
<script src="js/picker.date.js"></script>
<script src="js/zoom.js"></script>
<script src="js/main.js"></script>
<script>
		var pass_usu;
		var  pass_usur;
		var password;
		pass_usu = document.getElementById('pass_usu');
		pass_usur = document.getElementById('pass_usur');

		password.onchange = pass_usur.onkeyup = passwordMatch;

	function passwordMatch() {
    if(pass_usu.value !== pass_usur.value)
        pass_usur.setCustomValidity('Las contraseñas no coinciden.');
    else
        pass_usur.setCustomValidity('');
	}

</script>


</body>



</html>
<?php
	}else{
		echo '<script> window.location="index.php"; </script>';
	}
	$profile = $_SESSION['usuario'];
?>
