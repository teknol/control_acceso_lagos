<?php
	session_start();
	include 'serv.php';
	if(isset($_SESSION['usuario'])){
		echo "<div class='navbar-fixed'>
        <nav role='navigation'>
            <div style='background-color: #26a69a;'>
                <div class='nav-wrapper' style='background-color: #26a69a;'>
                <a href='#' id='logo-container' class='brand-logo center flow-text'>Trabajos</a>
                    <ul id='slide-out' class='side-nav'>
                        <li><a href='logout.php' style='color: #26a69a;'>Salir</a></li>
                    </ul>
                <a href='#' data-activates='slide-out' class='button-collapse show-on-large' id='dale'><i class='mdi-navigation-menu'></i></a>
                </div>
            </div>
        </nav>
    </div>";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Perfil</title>
	<meta charset="UTF-8">
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="css/materialize.css" media="screen,projection">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/datepicker.css">
	<link rel="stylesheet" type="text/css" href="css/zoom.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>
 <!--Navigation-->
 

<div class="row">
	<div class="col s12">
		<ul class="tabs">
			<li class="tab col6"><a href="#formus">Registro</a></li>
			<li class="tab col6"><a href="#lista">Trabajadores</a></li>
		</ul>
	</div>
</div>
<div class="container" id="formus">
		<form action="registro_alba.php" class="col s12" method="post" enctype="multipart/form-data">
			<div class="input-field col s12 m6">
				<input type="text" id="trabajador" name="nombre" required>
				<label for="trabajador">Trabajador</label>
			</div>
			<div class="input-field col s12 m6">
				<input type="text" id="ocupacion" name="ocupacion" required>
				<label for="ocupacion">Ocupacion</label>
			</div>
			<div class="row">
				<div class="input-field col s12 m12">
				    <select name="residencia" required id="resident">
				      <option value="" disabled selected>Residencia</option>
				      <option value="Lago Escondido">Lago Escondido</option>
				      <option value="Los Lagos">Los Lagos</option>
				    </select>
	    			<label></label>
	  			</div>
			</div>

			<div class="row">
				<div class="input-field col s6 m6" id="slagos" hidden>
				    <select name="manzana">
				      <option value="" disabled selected>Manzana</option>
				      <option value="1A">1A</option>
				      <option value="1B">1B</option>
				      <option value="1C">1C</option>
				      <option value="2A">2A</option>
				      <option value="2B">2B</option>
				      <option value="2C">2C</option>
				      <option value="3A">3A</option>
				      <option value="3B">3B</option>
				      <option value="3C">3C</option>
				      <option value="4A">4A</option>
				      <option value="4B">4B</option>
				      <option value="4C">4C</option>
				      <option value="5A">5A</option>
				      <option value="5B">5B</option>
				      <option value="5C">5C</option>
				      <option value="6A">6A</option>
				      <option value="6B">6B</option>
				      <option value="6C">6C</option>
				    </select>
				  </div>
				  <div class="input-field col s6 m6"  id="sescond" hidden>
				    <select name="manzana">
				     <option value="" disabled selected>Manzana</option>
				      <option value="7A">7A</option>
				      <option value="7B">7B</option>
				      <option value="7C">7C</option>
				      <option value="7D">7D</option>
				      <option value="7E">7E</option>
				      <option value="7F">7F</option>
				    </select>
	    			<label></label>
	  			</div>
				<div class="input-field col s6 m6">
				    <select name="lote">
				      <option value="" disabled selected>Lote</option>
				      <option value="1">1</option>
				      <option value="2">2</option>
				      <option value="3">3</option>
				      <option value="4">4</option>
				      <option value="5">5</option>
				      <option value="6">6</option>
				      <option value="7">7</option>
				      <option value="8">8</option>
				      <option value="9">9</option>
				      <option value="10">10</option>
				      <option value="11">11</option>
				      <option value="12">12</option>
				      <option value="13">13</option>
				      <option value="14">14</option>
				      <option value="15">15</option>
				      <option value="16">16</option>
				      <option value="17">17</option>
				      <option value="18">18</option>
				      <option value="19">19</option>
				      <option value="20">20</option>
				      <option value="21">21</option>
				      <option value="22">22</option>
				      <option value="23">23</option>
				      <option value="24">24</option>
				      <option value="25">25</option>
				      <option value="26">26</option>
				      <option value="27">28</option>
				      <option value="28">28</option>
				      <option value="29">29</option>
				      <option value="30">30</option>
				      <option value="31">31</option>
				      <option value="32">32</option>
				      <option value="33">33</option>
				      <option value="34">34</option>
				      <option value="35">35</option>
				      <option value="36">36</option>
				      <option value="37">37</option>
				      <option value="38">38</option>
				      <option value="39">39</option>
				      <option value="40">40</option>
				      <option value="41">41</option>
				      <option value="42">42</option>
				      <option value="43">43</option>
				      <option value="44">44</option>
				      <option value="45">45</option>
				      <option value="46">46</option>
				      <option value="47">47</option>
				      <option value="48">48</option>
				      <option value="49">49</option>
				      <option value="50">50</option>
				      <option value="51">51</option>
				      <option value="52">52</option>
				      <option value="53">53</option>
				      <option value="54">54</option>
				      <option value="55">55</option>
				      <option value="56">56</option>
				      <option value="57">57</option>
				      <option value="58">58</option>
				      <option value="59">59</option>
				      <option value="60">60</option>
				      <option value="61">61</option>
				      <option value="62">62</option>
				      <option value="63">63</option>
				      <option value="64">64</option>
				      <option value="65">65</option>
				      <option value="66">66</option>
				      <option value="67">67</option>
				    </select>
	    			<label></label>
	  			</div>
			</div>
			<div class="row">
				<div class="col s12 m6">
					<div class="file-field input-field">
      					<div class="btn">
        					<span><i class="material-icons">camera</i></span>
        					<input type="file" name="anverife" id="anverife">
      					</div>
				      <div class="file-path-wrapper">
				       	<input class="file-path validate" name="anverife" type="text" value="IFE Anverso">
				      </div>
    				</div>
				</div>
				<div class="col s12 m6">
					<div class="file-field input-field">
      					<div class="btn">
        					<span><i class="material-icons">camera</i></span>
        					<input type="file" name="reverife" id="reverife">
      					</div>
				      <div class="file-path-wrapper">
				        <input class="file-path validate" type="text" value="IFE Reverso" name="reverife">
				      </div>
    				</div>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m12">
					<div class="file-field input-field">
      					<div class="btn">
        					<span><i class="material-icons">camera</i></span>
        					<input type="file" name="fotografia">
      					</div>
				      <div class="file-path-wrapper">
				        <input class="file-path validate" type="text" value="Fotografía">
				      </div>
    				</div>
				</div>
			</div>
			<div class="row">
				  <input type="date" class="datepicker" name="fecha_inicio" value="Fecha de Inicio">
			</div>
			<div class="row">
				  <input type="date" class="datepicker" name="fecha_final" value="Fecha Final">
			</div>
		
		<div class="row">
			<button type="submit" class="waves-effect waves-light btn" style="float: left;">Registro<i class="material-icons right">send</i></button>
			<a class="waves-effect waves-light btn red" href="logout.php" style="float: right;">Salir<i class="material-icons right">exit_to_app</i></a>
		</div>
		</form>
</div>

	<div class="row" id="lista">
		<table class="responsive-table bordered striped" id="tablaemp2">
			<tr>
				<th>Trabajador</th>
				<th>Ocupación</th>
				<th>Residencia</th>
				<th>Manzana</th>
				<th>Lote</th>
				<th>IFE Anverso</th>
				<th>IFE Reverso</th>
				<th>Fotografía</th>
				<th class="red" style="color:white;">Eliminar</th>
			</tr>	
			<?php
				$sql = "SELECT usuarios.usuario,albaniles.id_albanil, albaniles.albanil,albaniles.ocupacion,albaniles.residencia,albaniles.manzana,albaniles.lote,albaniles.ife_anverso, albaniles.ife_reverso, albaniles.fotografia FROM usuarios INNER JOIN albaniles ON albaniles.id_usuario = usuarios.id_usuario WHERE usuarios.usuario = '".$_SESSION['usuario']."'";
				//$sql_delete = "DELETE FROM albaniles WHERE id_albanil=".$row['id_albanil'];
				$result = mysqli_query($connect,$sql);
				while($row = mysqli_fetch_array($result)){
			?>
			<tr>
				<td><? echo $row['albanil']; ?></td>
				<td><? echo $row['ocupacion']; ?></td>
				<td><? echo $row['residencia'];?></td>
				<td><? echo $row['manzana']; ?></td>
				<td><? echo $row['lote']; ?></td>
				<td><img class="responsive-img mevoy" width="100" height="100" data-action="zoom" src="<?php echo $row['ife_anverso'];?>"></td>
				<td><img class="responsive-img mevoy" width="100" height="100" data-action="zoom" src="<?php echo $row['ife_reverso'];?>"></td>
				<td><img class="responsive-img mevoy" width="100" height="100" data-action="zoom" src="<?php echo $row['fotografia'];?>"></td>
				<td><button style="position: relative;" class="delete_class2 btn red" id="<? echo $row['id_albanil']; ?>"><i class="material-icons">delete</i></button></td>
			</tr>

		<? } ?></table>
</div>



<div class="container">
	<div class="row">
		
	</div>	
</div>
	
</body>
<script src="js/jquery-2.2.2.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/picker.js"></script>
<script src="js/picker.time.js"></script>
<script src="js/picker.date.js"></script>
<script src="js/zoom.js"></script>
<script src="js/main.js"></script>
	<script>
		$("#resident").change(function(){
			if($(this).val() === 'Los Lagos'){
				$("#slagos").show();
				$("#sescond").hide();
			}
			if($(this).val() === 'Lago Escondido'){
				$("#sescond").show();
				$("#slagos").hide();
			}
		})
	</script>
</html>
<?php
	}else{
		echo '<script> window.location="index.php"; </script>';
	}
	$profile = $_SESSION['usuario'];
?>
