<?php 
	require("../serv.php");
?>
	<div class="container">
		<div class="row">
		<div class="center flow-text" style="font-size: 22px;">Ingenieros Registrados</div>
			<table class="responsive-table bordered  striped" id="tabla">
				<tr id="thistr">
					<th>Ingeniero</th>
					<th>nick de usuario</th>
					<th>Correo de contacto</th>
					<th>Editar</th>
					<th>Eliminar</th>
				</tr>
				<?php
					//$sql = "SELECT id_usuario,usuario FROM usuarios WHERE tipo=1";
					$sql = "SELECT id_usuario,nombre_usuario,usuario,correo FROM usuarios  WHERE tipo=1";
					$result = mysqli_query($connect,$sql);
					while($row = mysqli_fetch_array($result)){
						?>
						<tr>
							<td class="user_name"><? echo $row['nombre_usuario'];?></td>
							<td class="nick_usuario"><? echo $row['usuario'];?></td>
							<td class="correo_usu"><? echo $row['correo']?></td>
							<td>
								<button id="edit_class" data-target="modal1" class="modal-trigger btn blue" value="<? echo $row['id_usuario']; ?>"><i class="material-icons">edit</i></button>
							</td>
							<td>
								<button class="btn red" value="<? echo $row['id_usuario']; ?>" id="delete_class_usuario"><i class="material-icons">delete</i></button>
							</td>
						</tr>
					<?}?>
			</table>
		</div>
	</div>
		<div class="container">
		<div class="row">
		<div class="center flow-text" style="font-size: 22px;">Familias Registradas</div>
			<table class="responsive-table bordered  striped">
				<tr>
					<th>Familia</th>
					<th>Nick de la familia</th>
					<th hidden>Editar</th>
					<th>Correo de Contacto</th>
					<th>Editar</th>
					<th>Eliminar</th>
				</tr>
				<?php
					//$sql = "SELECT id_usuario,usuario FROM usuarios WHERE tipo=1";
					$sql = "SELECT id_usuario,nombre_usuario,usuario,correo FROM usuarios  WHERE tipo=3";
					$result = mysqli_query($connect,$sql);
					while($row = mysqli_fetch_array($result)){
						?>
						<tr>
							<td class="user_name"><? echo $row['nombre_usuario']; ?></td>
							<td class="nick_usuario"><? echo $row['usuario'];?></td>
							<td class="correo_usu"><?echo $row['correo'];?></td>
							<td>
								<button id="edit_classfa" data-target="modal1" class="modal-trigger btn blue" value="<? echo $row['id_usuario']; ?>"><i class="material-icons">edit</i></button>
							</td>
							<td>
								<button class="btn red" value="<? echo $row['id_usuario']; ?>" id="delete_class_usuario"><i class="material-icons">delete</i></button>

							
							</td>
						</tr>
					<?}?>
			</table>
		</div>
	</div>

