<?php
 include_once ('fpdf/fpdf.php');
class PDF extends FPDF{

    function designUp($espejo = 0){
        
        $this->Rect( (13 + $espejo), 13, 90, 100); //Marco exterior
        $this->Rect( (18 + $espejo), 18, 25, 30); //Marco foto
        //folio
        $this->setXY((68 + $espejo), 20);
        $this->Cell(29, 7, 'Folio');
        $this->setXY((78 + $espejo), 20);
        $this->Cell(22, 7, '', 1);
        //Nombre y su recuadro
        $this->setXY((50 + $espejo), 23);
        $this->Cell(29, 7, 'Empleado');
        $this->setXY((50 + $espejo), 30);
        $this->Cell(50, 7, '', 1);
 
        //Ocupacion y su recuadro
        $this->setXY((50 + $espejo), 38);
        $this->Cell(29, 7, 'Ocupacion');
        $this->setXY((50 + $espejo), 45);
        $this->Cell(50, 7, '', 1);
       //ingeniero que autoriza
        $this->setXY((13 + $espejo), 62);
        $this->Cell(29, 7, 'Persona que autoriza');
        $this->setXY((20 + $espejo), 70);
        $this->Cell(80, 7, '', 1);
        //manzana
        $this->setXY((13 + $espejo), 80);
        $this->Cell(29, 7, 'Manzana');
        $this->setXY((15 + $espejo), 85);
        $this->Cell(20, 7, '', 1);
           //faltas al reglamento
        $this->setXY((13 + $espejo), 95);
        $this->Cell(29, 7, 'Faltas al reglamento');
        $this->setXY((15 + $espejo), 100);
        $this->Cell(7, 7, '1', 1);
        //faltas al reglamento2
        $this->setXY((24 + $espejo), 100);
        $this->Cell(7, 7, '2', 1);
        //faltas al reglamento3
        $this->setXY((33 + $espejo), 100);
        $this->Cell(7, 7, '3', 1);
        //lote
        $this->setXY((75 + $espejo), 80);
        $this->Cell(29, 7, 'Lote');
        $this->setXY((75 + $espejo), 85);
        $this->Cell(25, 7, '', 1);
        //fechas final
         $this->setXY((55 + $espejo), 100);
        $this->Cell(29, 7, 'fecha final');
        $this->setXY((75 + $espejo), 100);
        $this->Cell(25, 7, '', 1);
        //fechas inicio
         $this->setXY((54 + $espejo), 93);
        $this->Cell(29, 7, 'fecha inicio');
        $this->setXY((75 + $espejo), 93);
        $this->Cell(25, 7, '', 1);
         //mensaje
        $this->setXY((110 + $espejo), 20);
        $this->Cell(29, 7, 'ESTA CREDENCIAL ES INDIVIDUAL E');
        $this->setXY((110 + $espejo), 20);
        $this->Cell(29, 14, 'INTRANSFERIBLE.');
        $this->setXY((110 + $espejo), 20);
        $this->Cell(29, 21, 'EN CASO DE DEJAR DE PRESTAR MIS ');
        $this->setXY((110 + $espejo), 20);
        $this->Cell(29, 28, 'SERVICIOS ME COMPROMETO A HACER');
        $this->setXY((110 + $espejo), 20);
        $this->Cell(29, 35, 'DEVOLUCION INMEDIATA DE LA MISMA.');
        $this->setXY((110 + $espejo), 20);
        $this->Cell(29, 42, '');
        $this->setXY((110 + $espejo), 20);
        $this->Cell(75, 30, '', 1);

 
        //Edad y su recuadro
       /* $this->setXY( (18 + $espejo), 53);
        $this->Cell(13, 7, 'Edad');
 
        $this->setXY((31 + $espejo), 53);
        $this->Cell(12, 7, '', 1);*/
 
        //Imagen de expo
        /*$this->Image('images/logo.jpg', (18 + $espejo), 65, 80 ,45);*/
 
    }
 
    function designDown($espejo = 0){
        $this->Rect((13 + $espejo), 13, 180, 100); //Marco exterior
        //$this->Rect( (18 + $espejo), 18 , 150, 30); //Marco foto
 
        //Nombre
       /* $this->setXY((50 + $espejo), 40);
        $this->Cell(29, 7, 'Nombre');
 
        $this->setXY((50 + $espejo), 48);
        $this->Cell(50, 7, '', 1);
 
        //Mail
        $this->setXY((50 + $espejo), 65);
        $this->Cell(29, 7, 'Mail');*/
 
        /*$this->setXY((50 + $espejo), 73);
        $this->Cell(50, 7, '', 1);
 
        //Edad
        $this->setXY((18 + $espejo), 73);
        $this->Cell(13, 7, 'Edad');
 
        $this->setXY((31 + $espejo), 73);
        $this->Cell(12, 7, '', 1);*/
 
        //Imagen de expo
        //$this->Image('images/logo.jpg', (18 + $espejo), 203, 80 ,45);
 
    }
}//fin clase PDF
?>	
