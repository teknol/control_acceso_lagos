<?php
	/*session_start();
	include 'serv.php';
	if(isset($_SESSION['usuario'])){
		echo 's<cript>window.location="panel.php";</script>';
	}*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>login de usuario</title>
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="css/materialize.css" media="screen,projection">
	<link rel="stylesheet" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>
   <nav>
    <div class="nav-wrapper" style="background-color: #26a69a;">
      <a href="#" class="brand-logo center flow-text">Control de Acceso</a>
      <ul id="nav-mobile" class="left hide-on-med-and-down">
        
      </ul>
    </div>
  </nav>
<div class="container" style="margin-top: 10px;">
	<div class="row">
		<div class="col s12 m12 l12">
			<img src="images/logouno.png" alt="logo" class="responsive-img centerlogo">
		</div>
	</div>
	<form action="validar.php" method="post" class="col s12">
		<div class="row">
			<div class="input-field col s12 m12">
				<input type="text" name="usuario" required id="first_name" class="validate">
				<label for="first_name">Usuario</label>
			</div>
			<div class="input-field col s12 m12">
				<input id="password" type="password" name="password" required>
				<label for="password">Contraseña</label>
			</div>
		</div>
		<div class="center">
			<button class="btn waves-effect waves-light" type="submit" name="login">Entrar
    			<i class="material-icons right">send</i>
  			</button>
		</div>
	</form>
	
</div>
<script src="js/jquery-2.2.2.min.js"></script>
<script src="js/materialize.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>