<?php
	session_start();

	include 'serv.php';
	if(isset($_SESSION['usuario'])){
		echo "<div class='navbar-fixed'>
        <nav role='navigation'>
            <div style='background-color: #26a69a;'>
                <div class='nav-wrapper' style='background-color: #26a69a;'>
                <a href='#' id='logo-container' class='brand-logo center flow-text'>Empleados</a>
                    <ul id='slide-out' class='side-nav'>
                        <li><a href='logout.php' style='color: #26a69a;'>Salir</a></li>
                    </ul>
                <a href='#' data-activates='slide-out' class='button-collapse show-on-large' id='dale'><i class='mdi-navigation-menu'></i></a>
                </div>
            </div>
        </nav>
    </div>";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Perfil</title>
	<meta charset="UTF-8">
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="css/materialize.css" media="screen,projection">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/datepicker.css">
	<link rel="stylesheet" type="text/css" href="css/zoom.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>


<div>
	<div class="container">
		<table class="responsive-table bordered striped" id="tablaemp">
				<tr>
					<th>Empleado</th>
					<th>Ocupación</th>
					<th>Calle</th>
					<th>Número</th>
					<th>Residencia</th>
					<th>IFE Anverso</th>
					<th>IFE Reverso</th>
					<th>Fotografía</th>
					<th>Fecha Final</th>
					<th class="red" style="color:white;">Eliminar</th>
					<th hidden>id</th>
				</tr>
			<?php
				$hoy = date('Y/m/d');
				$sql = "SELECT * FROM empleados WHERE fecha_inicio <= '$hoy' AND fecha_final <= '$hoy'";
				$result = mysqli_query($connect,$sql);
				while($row = mysqli_fetch_array($result)){
			?>
			
				<tr>
					<td><? echo $row['empleado']; ?></td>
					<td><? echo $row['ocupacion']; ?></td>
					<td><? echo $row['calle'];?></td>
					<td><? echo $row['numero'];?></td>
					<td><? echo $row['residencia'];?></td>
					<td><img class="responsive-img" width="150" height="150" data-action="zoom" src="<?php echo $row['ife_anverso'];?>"></td>
					<td><img class="responsive-img" width="150" height="150" data-action="zoom" src="<?php echo $row['ife_reverso'];?>"></td>
					<td><img class="responsive-img" width="150" height="150" data-action="zoom" src="<?php echo $row['fotografia'];?>"></td>
					<td><? echo $row['fecha_final']; ?></td>
					<td><button class="delete_class btn red" id="<? echo $row['id_empleado']; ?>"><i class="material-icons">delete</i></button></td>
				</tr>
		<?}	?></table>
	</div>
</div>
<div class="container" style="margin-top: 20px;">
	<a class="waves-effect waves-light btn" href="normal.php" style="float: right;">Volver al panel<i class="material-icons right">exit_to_app</i></a>
</div>
</body>



<script src="js/jquery-2.2.2.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/picker.js"></script>
<script src="js/picker.time.js"></script>
<script src="js/picker.date.js"></script>
<script src="js/zoom.js"></script>
<script src="js/main.js"></script>
</html>
<?php
	}else{
		echo '<script> window.location="index.php"; </script>';
	}
	$profile = $_SESSION['usuario'];
?>