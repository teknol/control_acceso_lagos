<?php
	session_start();
	include 'serv.php';
	if(isset($_SESSION['usuario'])){
		echo "<div class='navbar-fixed'>
        <nav role='navigation'>
            <div style='background-color: #26a69a;'>
                <div class='nav-wrapper' style='background-color: #26a69a;'>
                <a href='#' id='logo-container' class='brand-logo center flow-text'>Administración</a>
                    <ul id='slide-out' class='side-nav'>
                        <li><a href='logout.php' style='color: #26a69a;'>Salir</a></li>
                    </ul>
                <a href='#' data-activates='slide-out' class='button-collapse show-on-large' id='dale'><i class='mdi-navigation-menu'></i></a>
                </div>
            </div>
        </nav>
    </div>";
?>
<!DOCTYPE html>
<html>
<head>
<title></title>
    <meta charset="utf-8">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/materialize.css" media="screen,projection">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/zoom.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>
        <div class="container" style="margin-top: 10px;">
        <h3 class="flow-text center">Ingenieros-Arquitectos y Trabajadores</h3>
            <div class="row">
            <a href=""></a>
                <table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" class="responsive-table bordered  striped">
                <tr>
                    <th>Ingeniero</th>
                    <th>Consultar Trabajadores</th>
                </tr>
                <?php
                    //$sql = "SELECT id_usuario,usuario FROM usuarios WHERE tipo=1";
                    $sql = "SELECT id_usuario,nombre_usuario FROM usuarios WHERE tipo=1";
                    $result = mysqli_query($connect,$sql);
                    while($row = mysqli_fetch_array($result)){
                        
                        ?>
                        <tr>
                            <td><? echo $row['nombre_usuario']; ?></td>
                            <td>
                                <form method="post" action="consultando.php">
                                    <input hidden name="id_usuario" value="<? echo $row['id_usuario'];?>">
                                    <input class="waves-effect waves-light btn" type="submit" value="Ver">
                                </form>
                            </td>
                        </tr>
                    <?}?>
            </table>
            </div>
        </div>
<a class="waves-effect waves-light btn" href="normal.php" style="float: right; margin-top: 20px;">Volver al Inicio<i class="material-icons right">exit_to_app</i></a>
<script src="js/jquery-2.2.2.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/picker.js"></script>
<script src="js/picker.time.js"></script>
<script src="js/picker.date.js"></script>
<script src="js/zoom.js"></script>
<script src="js/main.js"></script>
</body>
</html>
<?php
    }else{
        echo '<script> window.location="index.php"; </script>';
    }
    $profile = $_SESSION['usuario'];
?>