<?php
	session_start();

	include 'serv.php';
	if(isset($_SESSION['usuario'])){
		echo "<div class='navbar-fixed'>
        <nav role='navigation'>
            <div style='background-color: #26a69a;'>
                <div class='nav-wrapper' style='background-color: #26a69a;'>
                <a href='#' id='logo-container' class='brand-logo center flow-text'>Empleados</a>
                    <ul id='slide-out' class='side-nav'>
                        <li><a href='logout.php' style='color: #26a69a;'>Salir</a></li>
                    </ul>
                <a href='#' data-activates='slide-out' class='button-collapse show-on-large' id='dale'><i class='mdi-navigation-menu'></i></a>
                </div>
            </div>
        </nav>
    </div>";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Perfil</title>
	<meta charset="UTF-8">
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="css/materialize.css" media="screen,projection">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/datepicker.css">
	<link rel="stylesheet" type="text/css" href="css/zoom.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>
<div class="row">
	<div class="col s12">
		<ul class="tabs">
			<li class="tab col s6"><a href="#regempleados">Registrar</a></li>
			<li class="tab col s6"><a href="#listaempleados">Lista de Empleados</a></li>
		</ul>
	</div>
</div>
<div id="regempleados" class="container">
	<div class="formfamilia">
		<form action="registro_empleados.php" class="col s12" method="post" enctype="multipart/form-data">
			<div class="input-field col s12 m6">
				<input type="text" id="empleado" name="nombre" required>
				<label for="empleado">Empleado</label>
			</div>
			<div class="input-field col s12 m6">
				<input type="text" id="ocupacion" name="ocupacion" required>
				<label for="ocupacion">Ocupacion</label>
			</div>
			<div class="row">
				<div class="input-field col s12 m6">
					<input type="text" id="calle" name="calle" required>
					<label for="calle">Calle</label>
				</div>
				<div class="input-field col s12 m6">
					<input type="text" id="numero" name="numero" required>
					<label for="numero">Número</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12 m12">
				    <select name="residencia">
				      <option value="" disabled selected>Residencia</option>
				      <option value="Lago Escondido">Lago Escondido</option>
				      <option value="Los Lagos">Los Lagos</option>
				    </select>
	    			<label></label>
	  			</div>
	  		</div>
			<div class="row">
				<div class="col s12 m6">
					<div class="file-field input-field">
      					<div class="btn">
        					<span><i class="material-icons">camera</i></span>
        					<input type="file" name="anverife" id="anverife">
      					</div>
				      <div class="file-path-wrapper">
				       	<input class="file-path validate" name="anverife" type="text" value="IFE Anverso">
				      </div>
    				</div>
				</div>
				<div class="col s12 m6">
					<div class="file-field input-field">
      					<div class="btn">
        					<span><i class="material-icons">camera</i></span>
        					<input type="file" name="reverife" id="reverife">
      					</div>
				      <div class="file-path-wrapper">
				        <input class="file-path validate" type="text" value="IFE Reverso" name="reverife">
				      </div>
    				</div>
				</div>
			</div>
			<div class="row">
				<div class="col s12 m12">
					<div class="file-field input-field">
      					<div class="btn">
        					<span><i class="material-icons">camera</i></span>
        					<input type="file" name="fotografia">
      					</div>
				      <div class="file-path-wrapper">
				        <input class="file-path validate" type="text" value="Fotografía">
				      </div>
    				</div>
				</div>
			</div>
			<div class="row">
				<div class="col s6 m6">
					<input type="time" name="horain" value="Entrada" class="timepicker">
				</div>
				<div class="col s6 m6">
					<input type="time" name="horaout" value="Salida" class="timepicker">
				</div>
			</div>	  
			</div>
			<div class="row">
				  <input type="date" class="datepicker" name="fecha_inicio" value="Fecha de Inicio">
			</div>
			<div class="row">
				  <input type="date" class="datepicker" name="fecha_final" value="Fecha Final">
			</div>
		
		<div class="row">
			<button type="submit" class="waves-effect waves-light btn">Registro<i class="material-icons right">send</i></button>
			<a href="logout.php" style="float: right;" class="waves-effect waves-light btn red">Salir<i class="material-icons right">exit_to_app</i></a>
		</div>
		</form>
	</div>
	
</div>
<div id="listaempleados">
	<div class="container">
		<table class="responsive-table bordered striped" id="tablaemp">
				<tr>
					<th>Empleado</th>
					<th>Ocupación</th>
					<th>Calle</th>
					<th>Número</th>
					<th>Residencia</th>
					<th>IFE Anverso</th>
					<th>IFE Reverso</th>
					<th>Fotografía</th>
					<th>Entrada</th>
					<th>Salida</th>
					<th>Fecha Inicio</th>
					<th>Fecha Final</th>
					<th class="red" style="color:white;">Eliminar</th>
				</tr>
			<?php
				$sql = "SELECT usuarios.usuario, empleados.empleado,empleados.horain,empleados.horaout,empleados.id_empleado, empleados.ocupacion,empleados.calle,empleados.numero,empleados.residencia,empleados.ife_anverso,empleados.ife_reverso,empleados.fotografia,empleados.fecha_inicio,empleados.fecha_final FROM usuarios INNER JOIN empleados ON empleados.id_usuario = usuarios.id_usuario WHERE usuarios.usuario = '".$_SESSION['usuario']."'";
				$result = mysqli_query($connect,$sql);
				while($row = mysqli_fetch_array($result)){
			?>
			
				<tr>
					<td><? echo $row['empleado']; ?></td>
					<td><? echo $row['ocupacion']; ?></td>
					<td><? echo $row['calle'];?></td>
					<td><? echo $row['numero'];?></td>
					<td><? echo $row['residencia'];?></td>
					<td><img class="responsive-img mevoy" width="150" height="150" data-action="zoom" src="<?php echo $row['ife_anverso'];?>"></td>
					<td><img class="responsive-img mevoy" width="150" height="150" data-action="zoom" src="<?php echo $row['ife_reverso'];?>"></td>
					<td><img class="responsive-img mevoy" width="150" height="150" data-action="zoom" src="<?php echo $row['fotografia'];?>"></td>
					<td><? echo $row['horain'];?></td>
					<td><? echo $row['horaout'];?></td>
					<td><? echo $row['fecha_inicio']; ?></td>
					<td><? echo $row['fecha_final']; ?></td>
					<td><button style="position: relative;" class="delete_class btn red" id="<? echo $row['id_empleado']; ?>"><i class="material-icons">delete</i></button></td>
				</tr>
		<?}	?></table>
	</div>
</div>
</body>



<script src="js/jquery-2.2.2.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/picker.js"></script>
<script src="js/picker.time.js"></script>
<script src="js/picker.date.js"></script>
<script src="js/zoom.js"></script>
<script src="js/main.js"></script>
</html>
<?php
	}else{
		echo '<script> window.location="index.php"; </script>';
	}
	$profile = $_SESSION['usuario'];
?>