-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 17, 2016 at 10:52 AM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Lagos`
--

-- --------------------------------------------------------

--
-- Table structure for table `albaniles`
--

CREATE TABLE IF NOT EXISTS `albaniles` (
  `id_albanil` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `albanil` varchar(45) NOT NULL,
  `ocupacion` varchar(45) NOT NULL,
  `residencia` varchar(45) NOT NULL,
  `manzana` varchar(45) NOT NULL,
  `lote` varchar(45) NOT NULL,
  `ife_anverso` varchar(200) NOT NULL,
  `ife_reverso` varchar(200) NOT NULL,
  `fotografia` varchar(200) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_final` date NOT NULL,
  `folio` int(11) NOT NULL,
  `clicks` int(11) NOT NULL,
  PRIMARY KEY (`id_albanil`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

-- --------------------------------------------------------

--
-- Table structure for table `empleados`
--

CREATE TABLE IF NOT EXISTS `empleados` (
  `id_empleado` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `empleado` varchar(45) NOT NULL,
  `ocupacion` varchar(45) NOT NULL,
  `residencia` varchar(45) NOT NULL,
  `calle` varchar(45) NOT NULL,
  `numero` varchar(45) NOT NULL,
  `ife_anverso` varchar(200) NOT NULL,
  `ife_reverso` varchar(200) NOT NULL,
  `fotografia` varchar(200) NOT NULL,
  `horain` time NOT NULL,
  `horaout` time NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_final` date NOT NULL,
  `clicks` int(11) NOT NULL,
  `folio` int(11) NOT NULL,
  PRIMARY KEY (`id_empleado`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) DEFAULT NULL,
  `nombre_usuario` varchar(45) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `correo` varchar(45) NOT NULL,
  `tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario`, `nombre_usuario`, `password`, `correo`, `tipo`) VALUES
(2, 'mario', 'Mario López', '202cb962ac59075b964b07152d234b70', 'lagos@mail.com', 2),
(7, 'famrdz', 'Familia Rodriguez', '81dc9bdb52d04dc20036dbd8313ed055', 'junior@mail.com', 3),
(8, 'famcaste', 'Familia Castelo', '81dc9bdb52d04dc20036dbd8313ed055', 'famcaste@hotmail.com', 3),
(13, 'albertored', 'Alberto Rojas', '202cb962ac59075b964b07152d234b70', 'albertod@gmail.com', 1),
(14, 'beto', 'Humberto LimÃ³n', '81dc9bdb52d04dc20036dbd8313ed055', 'soycodigo@gmail.com', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `albaniles`
--
ALTER TABLE `albaniles`
  ADD CONSTRAINT `usuario_albanil` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `empleados`
--
ALTER TABLE `empleados`
  ADD CONSTRAINT `emp_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
